# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Custom_reports',
    'summary': '',
    'category': 'DVKSP Customization',
    'author': 'ITMusketeers Consultancy Services',
    'website': 'http://www.itmsuketers.in',
    'description': """
""",
    'website': 'https://www.itmcs.com',
    'depends': ['base', 'account_accountant', 'dvksp_base'],
    'data': ['views/invoice_report.xml'
             ],
}
