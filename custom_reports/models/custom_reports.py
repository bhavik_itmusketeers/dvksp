from openerp import models, fields, api, _
import inflect


class report_invoice(models.AbstractModel):
    _name = 'report.custom_reports.report_invoice_custom'
    _template = 'custom_reports.report_invoice_custom'

    def get_buyer_order_date(self, origin):
        if origin:
            obj_purchase_order = self.env['purchase.order']
            purchase_id = obj_purchase_order.search([('name', '=', origin)])
            return purchase_id and purchase_id[0].date_order
        
    def get_amount_inwords(self, amount):
        p = inflect.engine()
        return p.number_to_words(amount).title()

    def bank_account_details(self, bank):
        bank_name = bank.bank_id and bank.bank_id.name or ''
        ifsc = bank.bank_id and bank.bank_id.bic
        acc_no = bank.acc_number
        return bank_name, ifsc, acc_no

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name(self._template)
        docs = self.env[report.model].browse(self._ids)
        docargs = {
            'get_buyer_order_date': self.get_buyer_order_date,
            'get_amount_inwords': self.get_amount_inwords,
            'bank_account_details': self.bank_account_details,
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': docs,
        }
        return report_obj.render(self._template, docargs)
