# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.\
# Copyright (C) 2016-Today ITMusketeers (<http://www.itmusketeers.in>

from openerp import api, fields, models, _
import openerp.addons.decimal_precision as dp


class ResPartner(models.Model):
    _inherit = "res.partner"

    is_agent = fields.Boolean('Is an Agent')
    agent_id = fields.Many2one('res.agent', string="Agent")

    @api.model
    def create(self, vals):
        if self._context.get('search_default_is_agent'):
            vals.update({'is_agent': True, 'customer': False})
        return super(ResPartner, self).create(vals)


class ResAgent(models.Model):
    _name = "res.agent"
    _inherits = {'res.partner': 'partner_id'}

    partner_id = fields.Many2one(
        'res.partner', string="Partner", auto_join=True, required=True, ondelete="restrict")

    @api.model
    def create(self, vals):
        if self._context.get('search_default_is_agent'):
            vals.update({'is_agent': True, 'customer': False})
        return super(ResAgent, self).create(vals)

    @api.multi
    def onchange_state(self, state_id):
        if state_id:
            state = self.env['res.country.state'].browse(state_id)
            return {'value': {'country_id': state.country_id.id}}
        return {'value': {}}


class SaleOrder(models.Model):
    _inherit = "sale.order"

    agent_id = fields.Many2one('res.agent', 'Agent', domain=[('is_agent', '=', True)],
                               readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, copy=False,)
    agent_pricelist_id = fields.Many2one('product.pricelist', string='Agent Pricelist',
                                         readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, copy=False,)

    @api.multi
    @api.onchange('agent_id')
    def onchange_agent_id(self):
        """
        Update the following fields when the agent is changed:
        - Pricelist
        """
        if not self.agent_id:
            self.update({
                'agent_pricelist_id': False,
            })
            return

        values = {
            'agent_pricelist_id': self.agent_id.property_product_pricelist and self.agent_id.property_product_pricelist.id or False,
        }
        self.update(values)

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if not self.partner_id:
            self.update({
                'partner_invoice_id': False,
                'partner_shipping_id': False,
                'payment_term_id': False,
                'fiscal_position_id': False,
                'agent_id': False
            })
            return

        addr = self.partner_id.address_get(['delivery', 'invoice'])
        values = {
            'pricelist_id': self.partner_id.property_product_pricelist and self.partner_id.property_product_pricelist.id or False,
            'payment_term_id': self.partner_id.property_payment_term_id and self.partner_id.property_payment_term_id.id or False,
            'partner_invoice_id': addr['invoice'],
            'partner_shipping_id': addr['delivery'],
            'agent_id': self.partner_id.agent_id and self.partner_id.agent_id.id
        }
        if self.env.user.company_id.sale_note:
            values['note'] = self.with_context(
                lang=self.partner_id.lang).env.user.company_id.sale_note

        if self.partner_id.user_id:
            values['user_id'] = self.partner_id.user_id.id
        if self.partner_id.team_id:
            values['team_id'] = self.partner_id.team_id.id
        self.update(values)

    @api.multi
    def _prepare_invoice(self):
        res = super(SaleOrder, self)._prepare_invoice()
        if self.agent_id:
            res.update(
                {'agent_id': self.agent_id and self.agent_id.id or False})
        return res


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    agent_price = fields.Float(
        'Agent Price', digits=dp.get_precision('Product Price'), default=0.0)

    @api.multi
    def _prepare_invoice_line(self, qty):
        res = super(SaleOrderLine, self)._prepare_invoice_line(qty)
        res.update({'agent_price': 0})
        if self.order_id.agent_id:
            res.update({'agent_price': self.agent_price})
        return res

    @api.onchange('product_id')
    def product_id_change(self):
        if not self.product_id:
            return {'domain': {'product_uom': []}}

        vals = {}
        domain = {
            'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        if not self.product_uom or (self.product_id.uom_id.category_id.id != self.product_uom.category_id.id):
            vals['product_uom'] = self.product_id.uom_id

        product = self.product_id.with_context(
            lang=self.order_id.partner_id.lang,
            partner=self.order_id.partner_id.id,
            quantity=self.product_uom_qty,
            date=self.order_id.date_order,
            pricelist=self.order_id.pricelist_id.id,
            uom=self.product_uom.id
        )

        name = product.name_get()[0][1]
        if product.description_sale:
            name += '\n' + product.description_sale
        vals['name'] = name

        self._compute_tax_id()

        if self.order_id.pricelist_id and self.order_id.partner_id:
            vals['price_unit'] = self.env['account.tax']._fix_tax_included_price(
                product.price, product.taxes_id, self.tax_id)
        if self.order_id.agent_id and self.order_id.agent_pricelist_id:
            agentproduct = self.product_id.with_context(
                lang=self.order_id.partner_id.lang,
                partner=self.order_id.agent_id.id,
                quantity=self.product_uom_qty,
                date=self.order_id.date_order,
                pricelist=self.order_id.agent_pricelist_id.id,
                uom=self.product_uom.id
            )
            vals['agent_price'] = agentproduct.price
        self.update(vals)
        return {'domain': domain}

    @api.onchange('product_uom', 'product_uom_qty')
    def product_uom_change(self):
        if not self.product_uom:
            self.price_unit = 0.0
            return
        if self.order_id.pricelist_id and self.order_id.partner_id:
            product = self.product_id.with_context(
                lang=self.order_id.partner_id.lang,
                partner=self.order_id.partner_id.id,
                quantity=self.product_uom_qty,
                date_order=self.order_id.date_order,
                pricelist=self.order_id.pricelist_id.id,
                uom=self.product_uom.id,
                fiscal_position=self.env.context.get('fiscal_position')
            )
            self.price_unit = self.env['account.tax']._fix_tax_included_price(
                product.price, product.taxes_id, self.tax_id)
        if self.order_id.agent_id and self.order_id.agent_pricelist_id:
            agentproduct = self.product_id.with_context(
                lang=self.order_id.partner_id.lang,
                partner=self.order_id.agent_id.id,
                quantity=self.product_uom_qty,
                date=self.order_id.date_order,
                pricelist=self.order_id.agent_pricelist_id.id,
                uom=self.product_uom.id
            )
            self.agent_price = agentproduct.price


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    agent_id = fields.Many2one('res.agent', domain=[('is_agent', '=', True)], string="Agent",
                               readonly=True, states={'draft': [('readonly', False)]}, copy=False,)

    @api.multi
    def action_agent_move_create(self):
        if self.agent_id:
            account_move = self.env['account.move']
            journal_obj = self.env['account.journal']
            ctx = dict(self._context, lang=self.partner_id.lang)
            agentexpense_journal_id = self.env['ir.model.data'].get_object_reference(
                'agent', "expenses_commission_journal")[1]
            agentexpense = journal_obj.browse(agentexpense_journal_id)
            iml = []
            for line in self.invoice_line_ids:
                if line.agent_price > 0:
                    debit_line = {'account_id': agentexpense.default_debit_account_id and agentexpense.default_debit_account_id.id,
                                  'credit': False,
                                  'debit': False,
                                  'price': (line.price_unit - line.agent_price) * line.quantity,
                                  'invoice_id': self.id,
                                  'name': line.name.split('\n')[0][:64],
                                  'partner_id': self.agent_id and self.agent_id.id,
                                  'product_id': line.product_id and line.product_id.id,
                                  'product_uom_id': line.uom_id and line.uom_id.id,
                                  'quantity': line.quantity,
                                  }
                    iml.append(debit_line)
                    credit_line = {'account_id': self.agent_id.property_account_payable_id and self.agent_id.property_account_payable_id.id,
                                   'credit': False,
                                   'price':-((line.price_unit - line.agent_price) * line.quantity),
                                   'debit': False,
                                   'invoice_id': self.id,
                                   'name': '/',
                                   'partner_id': self.agent_id and self.agent_id.id,
                                   'product_id': False,
                                   'product_uom_id': False,
                                   'quantity': line.quantity,
                                   }
                    iml.append(credit_line)
            agent = self.agent_id
            line = [(0, 0, self.line_get_convert(l, agent.partner_id.id))
                    for l in iml]
            line = self.group_lines(iml, line)

            line = self.finalize_invoice_move_lines(line)
            if line:
                move_vals = {
                    'ref': self.reference,
                    'line_ids': line,
                    'journal_id': agentexpense.id,
                    'date': self.date_invoice,
                    'narration': self.comment,
                }
                ctx['company_id'] = self.company_id.id
                ctx['dont_create_taxes'] = True
                ctx['invoice'] = self
                ctx_nolang = ctx.copy()
                ctx_nolang.pop('lang', None)
                move = account_move.with_context(ctx_nolang).create(move_vals)
                # Pass invoice in context in method post: used if you want to get the same
                # account move reference when creating the same invoice after a
                # cancelled one:
                move.post()
        return True


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    agent_price = fields.Float('Agent Price')


class account_payment(models.Model):
    _inherit = "account.payment"

    partner_type = fields.Selection(
        [('is_agent', 'Agent'), ('customer', 'Customer'), ('supplier', 'Vendor')])

    @api.multi
    def post(self):
        """ Create the journal items for the payment and update the payment's state to 'posted'.
            A journal entry is created containing an item in the source liquidity account (selected journal's default_debit or default_credit)
            and another in the destination reconciliable account (see _compute_destination_account_id).
            If invoice_ids is not empty, there will be one reconciliable move line per invoice to reconcile with.
            If the payment is a transfer, a second journal entry is created in the destination journal to receive money from the transfer account.
        """
        for rec in self:

            if rec.state != 'draft':
                raise UserError(
                    _("Only a draft payment can be posted. Trying to post a payment in state %s.") % rec.state)

            if any(inv.state != 'open' for inv in rec.invoice_ids):
                raise ValidationError(
                    _("The payment cannot be processed because the invoice is not open!"))

            # Use the right sequence to set the name
            if rec.payment_type == 'transfer':
                sequence_code = 'account.payment.transfer'
            else:
                if rec.partner_type == 'customer':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.customer.invoice'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.customer.refund'
                if rec.partner_type == 'supplier':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.supplier.refund'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.supplier.invoice'
                if rec.partner_type == 'is_agent':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.agent.in'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.agent.out'
            rec.name = self.env['ir.sequence'].with_context(
                ir_sequence_date=rec.payment_date).next_by_code(sequence_code)

            # Create the journal entry
            amount = rec.amount * \
                (rec.payment_type in ('outbound', 'transfer') and 1 or -1)
            move = rec._create_payment_entry(amount)

            # In case of a transfer, the first journal entry created debited the source liquidity account and credited
            # the transfer account. Now we debit the transfer account and
            # credit the destination liquidity account.
            if rec.payment_type == 'transfer':
                transfer_credit_aml = move.line_ids.filtered(
                    lambda r: r.account_id == rec.company_id.transfer_account_id)
                transfer_debit_aml = rec._create_transfer_entry(amount)
                (transfer_credit_aml + transfer_debit_aml).reconcile()

            rec.state = 'posted'
