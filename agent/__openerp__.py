# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.\
# Copyright (C) 2016-Today ITMusketeers (<http://www.itmusketeers.in>
{
    'name': 'Agent Management',
    'version': '1.0',
    'sequence': 4,
    'summary': 'Agent management for DVKSP',
    'category' : 'DVKSP Customization',
    'description': """
    Add Agent as partner ,
    Set Agent Price list in Sale order.
    Manage Payable and Receivables for Agents.
    Manage Payments for Agent.
    """,
    'author': 'ITMusketeers Consultancy Services',
    'website': 'http://www.itmsuketers.in',
    'depends': ['base', 'sale', 'account', 'l10n_in'],
    'data': [
            "agent.xml"
             ],
	'qweb': [
		],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
