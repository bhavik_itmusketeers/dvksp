from openerp import api, fields, models


class Respartner_consigner(models.Model):
    _inherit = "res.partner"

    is_consigner = fields.Boolean(string="Is a Consigner")


class RespurchaseOrder_consigner(models.Model):
    _inherit = "purchase.order"

    consigner_id = fields.Many2one(
        "res.partner", string="Consigner", domain=[('is_consigner', '=', True)])
