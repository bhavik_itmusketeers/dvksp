# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.\
# Copyright (C) 2016-Today ITMusketeers (<http://www.itmusketeers.in>

from openerp import api, fields, models, _

#----------------------------------------------------------
# Categories
#----------------------------------------------------------
class AccountAccountType(models.Model):
    _inherit = "account.account.type"

    parent_id = fields.Many2one('account.account.type', 'Parent Account Type')
    children_ids = fields.One2many('account.account.type', 'parent_id', 'Child Account Types')
