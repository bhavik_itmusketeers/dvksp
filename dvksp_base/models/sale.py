# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.\
# Copyright (C) 2016-Today ITMusketeers (<http://www.itmusketeers.in>

from openerp import api, fields, models, _
from openerp.fields import One2many, Many2one
from openerp.exceptions import UserError

#----------------------------------------------------------
# Quotation / Sale Order
#----------------------------------------------------------


class SaleOrder(models.Model):
    _inherit = "sale.order"

    branch_id = fields.Many2one('res.branch', string='Branch', required=True)

    @api.onchange("branch_id")
    def onchange_branch_id(self):
        warehouse_id = False
        if self.branch_id and self.branch_id.warehouse_ids and len(self.branch_id.warehouse_ids) == 1:
            warehouse_id = self.branch_id.warehouse_ids.id
        if self.branch_id and self.branch_id.warehouse_ids and len(self.branch_id.warehouse_ids) > 1:
            warehouse_id = [x.id for x in self.branch_id.warehouse_ids][0]
        self.warehouse_id = warehouse_id
        return

    @api.multi
    def _prepare_invoice(self):
        res = super(SaleOrder, self)._prepare_invoice()
        res.update(
            {'branch_id': self.branch_id and self.branch_id.id or False})
        return res


class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"

    @api.multi
    def _create_invoice(self, order, so_line, amount):
        inv_obj = self.env['account.invoice']
        ir_property_obj = self.env['ir.property']

        account_id = False
        if self.product_id.id:
            account_id = self.product_id.property_account_income_id.id
        if not account_id:
            prop = ir_property_obj.get(
                'property_account_income_categ_id', 'product.category')
            prop_id = prop and prop.id or False
            account_id = order.fiscal_position_id.map_account(prop_id)
        if not account_id:
            raise UserError(
                _('There is no income account defined for this product: "%s". You may have to install a chart of account from Accounting app, settings menu.') % 
                (self.product_id.name,))

        if self.amount <= 0.00:
            raise UserError(
                _('The value of the down payment amount must be positive.'))
        if self.advance_payment_method == 'percentage':
            amount = order.amount_untaxed * self.amount / 100
            name = _("Down payment of %s%%") % (self.amount,)
        else:
            amount = self.amount
            name = _('Down Payment')

        invoice = inv_obj.create({
            'name': order.client_order_ref or order.name,
            'branch_id': order.branch_id and order.branch_id.id or False,
            'origin': order.name,
            'type': 'out_invoice',
            'reference': False,
            'account_id': order.partner_id.property_account_receivable_id.id,
            'partner_id': order.partner_invoice_id.id,
            'invoice_line_ids': [(0, 0, {
                'name': name,
                'origin': order.name,
                'account_id': account_id,
                'price_unit': amount,
                'quantity': 1.0,
                'discount': 0.0,
                'uom_id': self.product_id.uom_id.id,
                'product_id': self.product_id.id,
                'sale_line_ids': [(6, 0, [so_line.id])],
                'invoice_line_tax_ids': [(6, 0, [x.id for x in self.product_id.taxes_id])],
                'account_analytic_id': order.project_id.id or False,
            })],
            'currency_id': order.pricelist_id.currency_id.id,
            'payment_term_id': order.payment_term_id.id,
            'fiscal_position_id': order.fiscal_position_id.id or order.partner_id.property_account_position_id.id,
            'team_id': order.team_id.id,
        })
        invoice.compute_taxes()
        return invoice


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    @api.multi
    def _compute_tax_id(self):
        for line in self:
            order = line.order_id
            partner = line.order_id.partner_id
            vat = cst = []
            if order.partner_id.state_id == order.branch_id.state_id:
                if partner.vat_no and partner.cst_no:
                    branch_tax_ids = line.product_id.categ_id.branch_tax_ids
                    for branch_tax in branch_tax_ids:
                        if (branch_tax.branch_id == order.branch_id and branch_tax.for_registered == True):
                            vat = [taxes.id for taxes in branch_tax.in_tax_ids]
                else:
                    branch_tax_ids = line.product_id.categ_id.branch_tax_ids
                    for branch_tax in branch_tax_ids:
                        if (branch_tax.branch_id == order.branch_id and branch_tax.for_registered != True):
                            vat = [taxes.id for taxes in branch_tax.in_tax_ids]
                line.tax_id = vat
            else:
                if partner.vat_no and partner.cst_no:
                    branch_tax_ids = line.product_id.categ_id.branch_tax_ids
                    for branch_tax in branch_tax_ids:
                        if (branch_tax.branch_id == order.branch_id and branch_tax.for_registered == True):
                            cst = [
                                taxes.id for taxes in branch_tax.out_tax_ids]
                else:
                    branch_tax_ids = line.product_id.categ_id.branch_tax_ids
                    for branch_tax in branch_tax_ids:
                        if (branch_tax.branch_id == order.branch_id and branch_tax.for_registered != True):
                            cst = [
                                taxes.id for taxes in branch_tax.out_tax_ids]
                line.tax_id = cst
