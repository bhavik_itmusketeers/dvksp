# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.\
# Copyright (C) 2016-Today ITMusketeers (<http://www.itmusketeers.in>

from openerp import api, fields, models, _


class res_partner(models.Model):
    _inherit = "res.partner"

    vat_no = fields.Char('VAT')
    cst_no = fields.Char('CST')
    pan_no = fields.Char('PAN')
    street3 = fields.Char('Street 3')


class res_users(models.Model):
    _inherit = "res.users"

    branch_id = fields.Many2one('res.branch', string="Related Branch")
