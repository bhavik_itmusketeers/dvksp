# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.\
# Copyright (C) 2016-Today ITMusketeers (<http://www.itmusketeers.in>

from openerp import api, fields, models, _

#----------------------------------------------------------
# Categories
#----------------------------------------------------------
class product_category(models.Model):
    _inherit = "product.category"

    branch_tax_ids = fields.One2many('branch.taxes', 'categ_id', string='Branch Taxes')

