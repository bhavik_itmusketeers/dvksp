# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.\
# Copyright (C) 2016-Today ITMusketeers (<http://www.itmusketeers.in>)

from openerp import api, fields, models, _

class branch_taxes(models.Model):
    _name = "branch.taxes"
    _description = 'Branch Taxes'

    branch_id = fields.Many2one('res.branch', 'Branch', required=True)
    in_tax_ids = fields.Many2many('account.tax', 'res_branch_tax_in_rel', 'bt_vat_id', 'tax_id', 'In-State taxes')
    out_tax_ids = fields.Many2many('account.tax', 'res_branch_tax_out_rel', 'bt_cst_id', 'tax_id', 'Out-State taxes')
    categ_id = fields.Many2one('product.category', 'Category')
    for_registered = fields.Boolean('Registered')

