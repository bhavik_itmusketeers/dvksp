# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.\
# Copyright (C) 2016-Today ITMusketeers (<http://www.itmusketeers.in>

from openerp import api, fields, models, _
import time
from openerp.exceptions import UserError
from openerp.exceptions import UserError, ValidationError
from openerp.tools.float_utils import float_compare, float_round


MAP_INVOICE_TYPE_PARTNER_TYPE = {
    'out_invoice': 'customer',
    'out_refund': 'customer',
    'in_invoice': 'supplier',
    'in_refund': 'supplier',
}


MAP_INVOICE_TYPE_PAYMENT_SIGN = {
    'out_invoice': 1,
    'in_refund': 1,
    'in_invoice': -1,
    'out_refund': -1,
}


class branch(models.Model):
    _name = "res.branch"
    _inherits = {'res.partner': 'partner_id'}

    partner_id = fields.Many2one(
        'res.partner', string="Partner", auto_join=True, required=True, ondelete="restrict")
    branch_code = fields.Char("Branch Code", required=True)
    warehouse_ids = fields.One2many(
        'stock.warehouse', 'branch_id', string="Related Warehouses")
    journal_ids = fields.One2many(
        'account.journal', 'branch_id', string="Related Invoices")

    @api.model
    def create(self, vals):
        user_obj = self.env['res.users']
        currency_id = user_obj.browse(
            self._context.get('uid')).company_id.currency_id.id
        branch_id = super(branch, self).create(vals)
        obj_acc_journal = self.env['account.journal']
        obj_stockwarehouse = self.env['stock.warehouse']
        name = vals.get('name', '')
        branch_code = vals.get('branch_code', '')
        tinvoice_journal_id = obj_acc_journal.create(
            {'name': 'Tax Invoice -' + str(name),
             'type': 'sale', 'code': 'TI' + branch_code, 'branch_id': branch_id.id, 'currency_id': currency_id})
        rinvoice_journal_id = obj_acc_journal.create(
            {'name': 'Retail Invoice -' + str(name),
             'type': 'sale', 'code': 'RI' + branch_code, 'branch_id': branch_id.id, 'currency_id': currency_id})
        hsinvoice_journal_id = obj_acc_journal.create(
            {'name': 'High Seas Invoice -' + str(name),
             'type': 'sale', 'code': 'HS' + branch_code, 'branch_id': branch_id.id, 'currency_id': currency_id})
        btinvoice_journal_id = obj_acc_journal.create(
            {'name': 'Branch Transfer Invoice -' + str(name),
             'type': 'sale', 'code': 'BT' + branch_code, 'branch_id': branch_id.id, 'currency_id': currency_id})
        intinvoice_journal_id = obj_acc_journal.create(
            {'name': 'International Invoice -' + str(name),
             'type': 'sale', 'code': 'INT' + branch_code, 'branch_id': branch_id.id, 'currency_id': currency_id})
        vendor_bill_journal_id = obj_acc_journal.create(
            {'name': 'Vender Bill journal-' + str(name),
             'type': 'purchase', 'code': 'BILL' + branch_code, 'branch_id': branch_id.id, 'currency_id': currency_id})
        warehouse_id = obj_stockwarehouse.create(
            {'name': 'Warehouse -' + str(name),
             'code': 'WH' + branch_code, 'branch_id': branch_id.id})
        tinvoice_journal_id.branch_id = branch_id.id,
        rinvoice_journal_id.branch_id = branch_id.id,
        hsinvoice_journal_id.branch_id = branch_id.id,
        btinvoice_journal_id.branch_id = branch_id.id,
        intinvoice_journal_id.branch_id = branch_id.id,
        vendor_bill_journal_id.branch_id = branch_id.id,
        warehouse_id.branch_id = branch_id.id,
        return branch_id

    @api.multi
    def unlink(self):
        for branch_id in self:
            [x.unlink() for x in branch_id.warehouse_ids]
            [x.unlink() for x in branch_id.journal_ids]
        return super(branch, self).unlink()


class stock_warehouse(models.Model):
    _inherit = "stock.warehouse"

    branch_id = fields.Many2one('res.branch', 'Branch')


class account_journal(models.Model):
    _inherit = "account.journal"

    branch_id = fields.Many2one('res.branch', 'Branch')
    code = fields.Char(string='Short Code', size=10, required=True,
                       help="The journal entries of this journal will be named using this prefix.")


class account_invoice(models.Model):
    _inherit = "account.invoice"

    branch_id = fields.Many2one('res.branch', 'Branch')
    bank_id = fields.Many2one('res.partner.bank', 'Bank Account')
    consigner = fields.Many2one(
        "res.partner", string="Consigner", domain=[('is_consigner', '=', True)])

    @api.onchange("partner_id", "branch_id")
    def onchange_branch(self):
        journal_id = False
        if self.type == "out_invoice":
            if self.partner_id.vat_no and self.partner_id.cst_no:
                journal_id = [x.id for x in self.branch_id.journal_ids if (
                    'TI' in x.code and x.currency_id == self.currency_id)]
            if not self.partner_id.vat_no or not self.partner_id.cst_no:
                journal_id = [x.id for x in self.branch_id.journal_ids if (
                    'RI' in x.code and x.currency_id == self.currency_id)]
            if self.partner_id.country_id and self.branch_id.country_id and (self.partner_id.country_id != self.branch_id.country_id):
                journal_id = [x.id for x in self.branch_id.journal_ids if (
                    'INT' in x.code and x.currency_id == self.currency_id)]
        elif self.type == "in_invoice":
            journal_id = [x.id for x in self.branch_id.journal_ids if ('BILL' in x.code and (
                x.currency_id.id == False or x.currency_id.id == self.currency_id.id))]
        self.journal_id = journal_id and journal_id[0] or False
        return

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        res = super(account_invoice, self)._onchange_partner_id()
        self.onchange_branch()
        return res

    @api.onchange('purchase_id')
    def purchase_order_change(self):
        if not self.purchase_id:
            return {}
        if not self.partner_id:
            self.partner_id = self.purchase_id.partner_id.id

        new_lines = self.env['account.invoice.line']
        for line in self.purchase_id.order_line:
            # Load a PO line only once
            if line in self.invoice_line_ids.mapped('purchase_line_id'):
                continue
            if line.product_id.purchase_method == 'purchase':
                qty = line.product_qty - line.qty_invoiced
            else:
                qty = line.qty_received - line.qty_invoiced
#             if float_compare(qty, 0.0, precision_rounding=line.product_uom.rounding) <= 0:
#                 qty = 0.0
            taxes = line.taxes_id
            invoice_line_tax_ids = self.purchase_id.fiscal_position_id.map_tax(
                taxes)
            data = {
                'purchase_line_id': line.id,
                'name': line.name,
                'origin': self.purchase_id.origin,
                'uom_id': line.product_uom.id,
                'product_id': line.product_id.id,
                'account_id': self.env['account.invoice.line'].with_context({'journal_id': self.journal_id.id, 'type': 'in_invoice'})._default_account(),
                'price_unit': line.order_id.currency_id.compute(line.price_unit, self.currency_id, round=False),
                'quantity': qty,
                'discount': 0.0,
                'account_analytic_id': line.account_analytic_id.id,
                'invoice_line_tax_ids': invoice_line_tax_ids.ids,
                'branch_id': self.purchase_id.branch_id.id,
            }
            account = new_lines.get_invoice_line_account(
                'in_invoice', line.product_id, self.purchase_id.fiscal_position_id, self.env.user.company_id)
            if account:
                data['account_id'] = account.id
            new_line = new_lines.new(data)
            new_line._set_additional_fields(self)
            new_lines += new_line
            branch_id = self.purchase_id.branch_id.id
            self.consigner = self.purchase_id.consigner_id and self.purchase_id.consigner_id.id
            self.branch_id = branch_id
            self.journal_id = self.onchange_branch()
        self.invoice_line_ids += new_lines
        self.purchase_id = False
        return {}
    # =====take branch_id in journal items and journal entry while valid button is clicked

    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_move = self.env['account.move']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise UserError(
                    _('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line_ids:
                raise UserError(_('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            if not inv.date_invoice:
                inv.with_context(ctx).write(
                    {'date_invoice': fields.Date.context_today(self)})
            date_invoice = inv.date_invoice
            company_currency = inv.company_id.currency_id

            # create move lines (one per invoice line + eventual taxes and
            # analytic lines)
            iml = inv.invoice_line_move_line_get()
            iml += inv.tax_line_move_line_get()

            diff_currency = inv.currency_id != company_currency
            # create one move line for the total and possibly adjust the other
            # lines amount
            total, total_currency, iml = inv.with_context(
                ctx).compute_invoice_totals(company_currency, iml)

            name = inv.name or '/'
            if inv.payment_term_id:
                totlines = inv.with_context(ctx).payment_term_id.with_context(
                    currency_id=inv.currency_id.id).compute(total, date_invoice)[0]
                res_amount_currency = total_currency
                ctx['date'] = date_invoice
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(
                            ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency

                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id,
                    })
            else:
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': total,
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                })
            part = self.env['res.partner']._find_accounting_partner(
                inv.partner_id)
            line = [(0, 0, self.line_get_convert(l, part.id)) for l in iml]
            line = inv.group_lines(iml, line)

            journal = inv.journal_id.with_context(ctx)
            line = inv.finalize_invoice_move_lines(line)

            date = inv.date or date_invoice
            branch = inv.branch_id
            move_vals = {
                'ref': inv.reference,
                'line_ids': line,
                'journal_id': journal.id,
                'date': date,
                'narration': inv.comment,
                'branch_id': branch.id,
            }
            ctx['company_id'] = inv.company_id.id
            ctx['dont_create_taxes'] = True
            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.with_context(ctx_nolang).create(move_vals)
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a
            # cancelled one:
            move.post()
            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'date': date,
                'move_name': move.name,
            }
            inv.with_context(ctx).write(vals)
        return True

        #===============validate while register payment (branch_id)


class account_payment(models.Model):
    _inherit = ['account.payment']

    branch_id = fields.Many2one('res.branch', 'Branch')

    @api.model
    def default_get(self, fields):
        rec = super(account_payment, self).default_get(fields)
        invoice_defaults = self.resolve_2many_commands(
            'invoice_ids', rec.get('invoice_ids'))
        if invoice_defaults and len(invoice_defaults) == 1:
            invoice = invoice_defaults[0]
            rec['communication'] = invoice['reference'] or invoice[
                'name'] or invoice['number']
            rec['currency_id'] = invoice['currency_id'][0]
            rec['payment_type'] = invoice['type'] in (
                'out_invoice', 'in_refund') and 'inbound' or 'outbound'
            rec['partner_type'] = MAP_INVOICE_TYPE_PARTNER_TYPE[
                invoice['type']]
            rec['partner_id'] = invoice['partner_id'][0]
            rec['amount'] = invoice['residual']
            rec['branch_id'] = invoice['branch_id']
        return rec

    def _create_payment_entry(self, amount):
        """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
            Return the journal entry.
        """
        aml_obj = self.env['account.move.line'].with_context(
            check_move_validity=False)
        invoice_currency = False
        if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
            # if all the invoices selected share the same currency, record the
            # paiement in that currency too
            invoice_currency = self.invoice_ids[0].currency_id
        debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(
            amount, self.currency_id, self.company_id.currency_id, invoice_currency)

        move = self.env['account.move'].create(self._get_move_vals())

        # Write line corresponding to invoice payment
        counterpart_aml_dict = self._get_shared_move_line_vals(
            debit, credit, amount_currency, move.id, False)
        counterpart_aml_dict.update(
            self._get_counterpart_move_line_vals(self.invoice_ids))
        counterpart_aml_dict.update({'currency_id': currency_id})
        counterpart_aml = aml_obj.create(counterpart_aml_dict)

        # Reconcile with the invoices
        if self.payment_difference_handling == 'reconcile' and self.payment_difference:
            writeoff_line = self._get_shared_move_line_vals(
                0, 0, 0, move.id, False)
            debit_wo, credit_wo, amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(
                self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)
            writeoff_line['name'] = _('Counterpart')
            writeoff_line['account_id'] = self.writeoff_account_id.id
            writeoff_line['debit'] = debit_wo
            writeoff_line['credit'] = credit_wo
            writeoff_line['amount_currency'] = amount_currency_wo
            writeoff_line['currency_id'] = currency_id
            writeoff_line = aml_obj.create(writeoff_line)
            if counterpart_aml['debit']:
                counterpart_aml['debit'] += credit_wo - debit_wo
            if counterpart_aml['credit']:
                counterpart_aml['credit'] += debit_wo - credit_wo
            counterpart_aml['amount_currency'] -= amount_currency_wo
        self.invoice_ids.register_payment(counterpart_aml)

        # Write counterpart lines
        if not self.currency_id != self.company_id.currency_id:
            amount_currency = 0
        liquidity_aml_dict = self._get_shared_move_line_vals(
            credit, debit, -amount_currency, move.id, False)
        liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
        aml_obj.create(liquidity_aml_dict)
        move.post()
        return move

    def _get_shared_move_line_vals(self, debit, credit, amount_currency, move_id, invoice_id=False):
        """ Returns values common to both move lines (except for debit, credit and amount_currency which are reversed)
        """
        return {
            'partner_id': self.payment_type in ('inbound', 'outbound') and self.env['res.partner']._find_accounting_partner(self.partner_id).id or False,
            'invoice_id': invoice_id and invoice_id.id or False,
            'move_id': move_id,
            'debit': debit,
            'credit': credit,
            'amount_currency': amount_currency or False,
            'branch_id': self.branch_id.id,
        }

    def _get_move_vals(self, journal=None):
        """ Return dict to create the payment move
        """
        journal = journal or self.journal_id
        if not journal.sequence_id:
            raise UserError(_('Configuration Error !'), _(
                'The journal %s does not have a sequence, please specify one.') % journal.name)
        if not journal.sequence_id.active:
            raise UserError(_('Configuration Error !'), _(
                'The sequence of journal %s is deactivated.') % journal.name)
        name = journal.with_context(
            ir_sequence_date=self.payment_date).sequence_id.next_by_id()
        return {
            'name': name,
            'date': self.payment_date,
            'ref': self.communication or '',
            'company_id': self.company_id.id,
            'journal_id': journal.id,
            'branch_id': self.branch_id.id,
        }

class account_move  (models.Model):
    _inherit = "account.move"

    branch_id = fields.Many2one('res.branch', 'Branch')

class account_move_line(models.Model):
    _inherit = "account.move.line"

    branch_id = fields.Many2one('res.branch', 'Branch', readonly=True, related='move_id.branch_id',
                                copy=False, store=True)


class purchase_order(models.Model):
    _inherit = "purchase.order"

    branch_id = fields.Many2one('res.branch', 'Branch', required=True)

    @api.onchange("branch_id")
    def onchange_branch_purchase(self):
        if self.branch_id:
            obj_picking_type = self.env["stock.picking.type"]
            warehouse_id = len(self.branch_id.warehouse_ids) == 1 and \
                self.branch_id.warehouse_ids or self.branch_id.warehouse_ids[0]
            picking_type_id = obj_picking_type.search(
                [("warehouse_id", "=", warehouse_id.id), ("code", "=", "incoming")])
            if picking_type_id and len(picking_type_id) == 1:
                self.picking_type_id = picking_type_id
                return

    @api.model
    def create(self, vals):
        order_id = super(purchase_order, self).create(vals)

        order_id.onchange_branch_purchase()
        return order_id

    @api.multi
    def write(self, vals):
        res = super(purchase_order, self).write(vals)
        if vals.get("branch_id"):
            self.onchange_branch_purchase()
        return res


class stock_quant(models.Model):
    _inherit = 'stock.quant'

    def _create_account_move_line(self, cr, uid, quants, move, credit_account_id, debit_account_id, journal_id, context=None):
        # group quants by cost
        quant_cost_qty = {}
        for quant in quants:
            if quant_cost_qty.get(quant.cost):
                quant_cost_qty[quant.cost] += quant.qty
            else:
                quant_cost_qty[quant.cost] = quant.qty
        move_obj = self.pool.get('account.move')
        for cost, qty in quant_cost_qty.items():
            move_lines = self._prepare_account_move_line(
                cr, uid, move, qty, cost, credit_account_id, debit_account_id, context=context)
            date = context.get(
                'force_period_date', fields.Date.context_today(quant))
            new_move = move_obj.create(cr, uid, {'journal_id': journal_id,
                                                 'line_ids': move_lines,
                                                 'date': date,
                                                 'ref': move.picking_id.name,
                                                 'branch_id': move.picking_type_id.warehouse_id.branch_id.id,
                                                 }, context=context)
            move_obj.post(cr, uid, [new_move], context=context)
