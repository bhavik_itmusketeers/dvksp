# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.\
# Copyright (C) 2016-Today ITMusketeers (<http://www.itmusketeers.in>
{
    'name': 'Base Customization For DVKSP',
    'version': '1.0',
    'sequence': 4,
    'summary': 'Basic Customization For DVKSP including Branch,Consignment',
    'category': 'DVKSP Customization',
    'description': """
    Manage Branch Wise Account Journals,warehouse,taxes etc...
    Manage Consginer
    """,
    'author': 'ITMusketeers Consultancy Services',
    'website': 'http://www.itmsuketers.in',
    'depends': ['stock', 'base', 'sale', 'account', 'account_accountant', 'purchase'],
    'data': ['data/account.account.type.csv',
             'data/account.account.csv',
             "security/branch_security.xml",
             "security/ir.model.access.csv",
             "views/branch_taxes_view.xml",
             "views/branch_view.xml",
             "views/product_category_view.xml",
             "views/res_view.xml",
             "views/res_view.xml",
             "views/consigner.xml",
             "views/account_view.xml"
             ],
    'qweb': [
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
