# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.\
# Copyright (C) 2016-Today ITMusketeers (<http://www.itmusketeers.in>
{
    'name': 'Custom Duty Management',
    'version': '1.0',
    'sequence': 4,
    'summary': 'Custom Duty Management For DVKSP',
    'category': 'DVKSP Customization',
    'description': """
    Manage Category(HSN Code) wise Custom Duties etc...
    """,
    'author': 'ITMusketeers Consultancy Services',
    'website': 'http://www.itmsuketers.in',
    'depends': ['base', 'product', 'purchase', 'dvksp_base', 'stock', 'stock_landed_costs'],
    'data': ['views/duty_view.xml',
             ],
    'qweb': [
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}

