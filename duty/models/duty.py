from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.tools import float_compare, float_round
from duplicity.tempdir import default


class Hsn_Custom_duty(models.Model):
    _name = 'hsn.custom.duty'

    duty_id = fields.Many2one('product.product', domain=[('landed_cost_ok', '=', True)], required=True)
    code = fields.Char(string="Duty Code")
    base_formula = fields.Char(string='Calculation Formula')
    rate = fields.Float("Duty Rate")
    category_id = fields.Many2one('product.category')


class Res_product_category(models.Model):
    _inherit = 'product.category'

    duty_ids = fields.One2many(
        'hsn.custom.duty', 'category_id')


class Purchase_order_line_custom(models.Model):
    _inherit = 'purchase.order.line'

    @api.depends('price_unit', 'product_qty', 'order_id.insu_rate',
                 'order_id.cer', 'order_id.load_rate', 'order_id.pack')
    def get_assessable_cost(self):
        duty_obj = self.env['duty.lines']
        for line in self:
            order = line.order_id
            total_qty = sum([x.product_qty for x in order.order_line])
            AV = 0.0
            if total_qty > 0:
                cer = line.order_id.cer
                inv_cost = line.product_qty * line.price_unit
                ac = ((line.order_id.pack * line.product_qty) / total_qty) + \
                    inv_cost
                inscost = (ac * cer) * (line.order_id.insu_rate / 100)
                loadcost = ((ac * cer) + inscost) * \
                    (line.order_id.load_rate / 100)
                AV = ac * cer + \
                    inscost + loadcost
            line.av = AV


    av = fields.Float(string="Assessable values", compute='get_assessable_cost')


class Purchase_order_custom(models.Model):
    _inherit = 'purchase.order'

    pack = fields.Float(string="Packaging Cost", digits=dp.get_precision('Product Price'))
    cer = fields.Float(string="Custom Exchange Rate", digits=dp.get_precision('Product Price'))
    insu_rate = fields.Float(string="Insurance Rate", digits=dp.get_precision('Product Price'), default=1.125)
    load_rate = fields.Float(string=" Loading Rate", digits=dp.get_precision('Product Price'), default=1)


class Duty_lines(models.Model):
    _name = 'duty.lines'

    line_id = fields.Many2one('purchase.order.line')
    landed_cost_lid = fields.Many2one('stock.landed.cost')
    duty_id = fields.Many2one('product.product', domain=[('landed_cost_ok', '=', True)], required=True)
    duty_amount = fields.Float(string='Duty Amount')


class Stock_Landed_Cost(models.Model):
    _inherit = 'stock.landed.cost'

    duty_line_ids = fields.One2many(
        'duty.lines', 'landed_cost_lid')
    picking_id = fields.Many2one('stock.picking', string='Picking', domain="[('state', '=', 'done')]", states={'done': [('readonly', True)]}, copy=False)
    
    @api.one
    def get_duties(self):
        duty_obj = self.env['duty.lines']
        self_obj = self.env['stock.landed.cost']
        cost_obj = self.env['stock.landed.cost.lines']
        duty_total = {}
        duty_obj.search([('landed_cost_lid', '=', self.id)]).unlink()
        cost_obj.search([('cost_id', '=', self.id)]).unlink()
        self.compute_landed_cost()
        for move in self.picking_id.move_lines:
            order_line = move.purchase_line_id
            AV = order_line.av
            for duty in order_line.product_id.categ_id.duty_ids:
                R = duty.rate / 100
                duty_code = str(duty.code)
                duty_amount = eval(duty.base_formula)
                exec("%s = %f" % (duty_code, duty_amount))
                duty_id = duty_obj.create({'landed_cost_id':self.id, 'line_id':order_line.id, 'duty_id': duty.duty_id.id, 'duty_amount': duty_amount})
                if duty_total.get(duty.duty_id.id):
                    duty_total[duty.duty_id.id] = duty_total[duty.duty_id.id] + duty_amount
                else:
                    duty_total[duty.duty_id.id] = duty_amount
        for product_id, cost in duty_total.iteritems():
            product_onchange = cost_obj.onchange_product_id(product_id)['value']
            product_onchange.update({'cost_id':self.id, 'product_id':product_id, 'price_unit':cost})
            cost_obj.create(product_onchange)
        self.compute_landed_cost()
        return True


    def get_valuation_lines(self, cr, uid, ids, picking_id=None, context=None):
        picking_obj = self.pool.get('stock.picking')
        lines = []
        if not picking_id:
            return lines

        picking = picking_obj.browse(cr, uid, picking_id)
        for move in picking.move_lines:
            # it doesn't make sense to make a landed cost for a product that isn't set as being valuated in real time at real cost
            if move.product_id.valuation != 'real_time' or move.product_id.cost_method != 'real':
                continue
            total_cost = 0.0
            weight = move.product_id and move.product_id.weight * move.product_qty
            volume = move.product_id and move.product_id.volume * move.product_qty
            for quant in move.quant_ids:
                total_cost += quant.cost * quant.qty
            vals = dict(product_id=move.product_id.id, move_id=move.id, quantity=move.product_qty, former_cost=total_cost, weight=weight, volume=volume)
            lines.append(vals)
        if not lines:
            raise UserError(_('The selected picking does not contain any move that would be impacted by landed costs. Landed costs are only possible for products configured in real time valuation with real price costing method. Please make sure it is the case, or you selected the correct picking'))
        return lines

    def compute_landed_cost(self, cr, uid, ids, context=None):
        line_obj = self.pool.get('stock.valuation.adjustment.lines')
        unlink_ids = line_obj.search(cr, uid, [('cost_id', 'in', ids)], context=context)
        line_obj.unlink(cr, uid, unlink_ids, context=context)
        digits = dp.get_precision('Product Price')(cr)
        towrite_dict = {}
        for cost in self.browse(cr, uid, ids, context=None):
            if not cost.picking_id:
                continue
            picking_id = cost.picking_id.id
            total_qty = total_cost = total_weight = total_volume = total_line = 0.0
            vals = self.get_valuation_lines(cr, uid, [cost.id], picking_id=picking_id, context=context)
            for v in vals:
                for line in cost.cost_lines:
                    v.update({'cost_id': cost.id, 'cost_line_id': line.id})
                    self.pool.get('stock.valuation.adjustment.lines').create(cr, uid, v, context=context)
                total_qty += v.get('quantity', 0.0)
                total_cost += v.get('former_cost', 0.0)
                total_weight += v.get('weight', 0.0)
                total_volume += v.get('volume', 0.0)
                total_line += 1

            for line in cost.cost_lines:
                value_split = 0.0
                for valuation in cost.valuation_adjustment_lines:
                    value = 0.0
                    if valuation.cost_line_id and valuation.cost_line_id.id == line.id:
                        if line.split_method == 'by_quantity' and total_qty:
                            per_unit = (line.price_unit / total_qty)
                            value = valuation.quantity * per_unit
                        elif line.split_method == 'by_weight' and total_weight:
                            per_unit = (line.price_unit / total_weight)
                            value = valuation.weight * per_unit
                        elif line.split_method == 'by_volume' and total_volume:
                            per_unit = (line.price_unit / total_volume)
                            value = valuation.volume * per_unit
                        elif line.split_method == 'equal':
                            value = (line.price_unit / total_line)
                        elif line.split_method == 'by_current_cost_price' and total_cost:
                            per_unit = (line.price_unit / total_cost)
                            value = valuation.former_cost * per_unit
                        else:
                            value = (line.price_unit / total_line)

                        if digits:
                            value = float_round(value, precision_digits=digits[1], rounding_method='UP')
                            fnc = min if line.price_unit > 0 else max
                            value = fnc(value, line.price_unit - value_split)
                            value_split += value

                        if valuation.id not in towrite_dict:
                            towrite_dict[valuation.id] = value
                        else:
                            towrite_dict[valuation.id] += value
        if towrite_dict:
            for key, value in towrite_dict.items():
                line_obj.write(cr, uid, key, {'additional_landed_cost': value}, context=context)
        return True


class Product_Product(models.Model):
    _inherit = 'product.product'
    
    is_duty = fields.Boolean('Duty')
