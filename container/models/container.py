from openerp import api, fields, models, _
from openerp.exceptions import UserError


class put_in_container_msg(models.Model):
    _inherit = "stock.picking"

    def put_in_pack(self, cr, uid, ids, context=None):
        stock_move_obj = self.pool["stock.move"]
        stock_operation_obj = self.pool["stock.pack.operation"]
        package_obj = self.pool["stock.quant.package"]
        package_id = False
        for pick in self.browse(cr, uid, ids, context=context):
            operations = [x for x in pick.pack_operation_ids if x.qty_done > 0 and (
                not x.result_package_id)]
            pack_operation_ids = []
            for operation in operations:
                # If we haven't done all qty in operation, we have to split
                # into 2 operation
                op = operation
                if operation.qty_done < operation.product_qty:
                    new_operation = stock_operation_obj.copy(cr, uid, operation.id, {
                                                             'product_qty': operation.qty_done, 'qty_done': operation.qty_done}, context=context)

                    stock_operation_obj.write(cr, uid, operation.id, {
                                              'product_qty': operation.product_qty - operation.qty_done, 'qty_done': 0}, context=context)
                    if operation.pack_lot_ids:
                        packlots_transfer = [(4, x.id)
                                             for x in operation.pack_lot_ids]
                        stock_operation_obj.write(
                            cr, uid, [new_operation], {'pack_lot_ids': packlots_transfer}, context=context)

                    op = stock_operation_obj.browse(
                        cr, uid, new_operation, context=context)
                pack_operation_ids.append(op.id)
            if operations:
                stock_operation_obj.check_tracking(
                    cr, uid, pack_operation_ids, context=context)
                package_id = package_obj.create(cr, uid, {}, context=context)
                stock_operation_obj.write(
                    cr, uid, pack_operation_ids, {'result_package_id': package_id}, context=context)
            else:
                raise UserError(
                    _('Please process some quantities to put in the Container first!'))
        return package_id
