# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Container Management',
    'summary': '',
    'category': 'DVKSP Customization',
    'summary': 'Container management for DVKSP',
    'author': 'ITMusketeers Consultancy Services',
    'website': 'http://www.itmsuketers.in',
    'description': """
    Container management for Goods
""",
    'depends': ['base', 'stock', 'purchase'],
    'data': ['views/container_view.xml'
             ],
}
